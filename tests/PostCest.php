<?php

class PostCest
{
    public function tryToTest(AcceptanceTester $I)
    {
        $I->login();
        $I->amOnPage('/post');
        $I->fillField(['css' => 'input[name="title"]'], 'Testowy artykuł 123');
        $I->click(['css' => 'input[type="submit"]']);
        $I->wait(1);
        $I->amOnPage('/posts');
        $I->see('Testowy artykuł 123');
        $I->click(['css' => 'a[href="http://localhost/article/testowy-artykul-123"]']);
        $I->waitForText('Display Comments');
        $I->seeInCurrentUrl('/article/testowy-artykul-123');

        $I->fillField(['css' => 'input[name="comment"]:first-of-type'], 'Testowy komentarz');
        $I->click(['css' => 'input[type="submit"]']);
        $I->waitForText('Testowy komentarz');
        $I->seeInDatabase('comments', ['comment' => 'Testowy komentarz']);
    }
}
